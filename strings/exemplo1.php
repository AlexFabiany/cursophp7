<?php
    $nome = "Alex";
    $sobrenome = 'Fabiany';
    $completo = "alex fabiany";

    echo "Nome e Sobrenome: $nome $sobrenome";
    echo "<br>";
    
    echo strtoupper($completo);
    echo "<br>";

    echo strtolower($completo);
    echo "<br>";

    echo ucwords($completo);
    echo "<br>";

    echo ucfirst($completo);
    echo "<br>";

    $completo = str_replace("e", "3", $completo);
    $completo = str_replace("a", "4", $completo);
    echo $completo;
    echo "<br>";

    $frase = "A repetição é mãe da retenção";
    echo "Frase => $frase";
    echo "<br>";
    $palavra = "mãe";
    $q = strpos($frase, $palavra);
    $texto = substr($frase, 0, $q);
    
    var_dump($texto);
    
    $texto2 = substr($frase, $q + strlen($palavra), strlen($frase));
    echo "<br>";
    var_dump($texto2);
?>