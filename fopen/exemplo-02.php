<?php 

	require_once("config.php");

	$filename = "usuarios.csv";

	$sql = new Sql();

	$usuarios = $sql->select("select * from tb_usuarios order by login");

	$headers = array();

	foreach ($usuarios[0] as $key => $value) {
		array_push($headers, ucfirst($key));
	}

  $file = fopen($filename, "w+");
  
  fwrite($file, implode(";", $headers) . "\r\n");

  foreach ($usuarios as $row) {

  	$dados = array();

  	foreach ($row as $key => $value) {

  		array_push($dados, $value);

  	}

  	fwrite($file,	implode(";", $dados) . "\r\n");

  }

	fclose($file);

	echo "Arquivo $filename criado com sucesso!";

?>