<?php
  $data = new DateTime();
  $periodo = new DateInterval("P10D");

  echo $data->format("d/m/Y h:i:s");
  echo "<br>";

  $data->add($periodo);

  echo $data->format("d/m/Y h:i:s");
  echo "<br>";
?>