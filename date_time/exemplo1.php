<?php

echo "Data/Hora local: " . date("d/m/Y H:i:s");
echo "<br><br>";

$ts = strtotime("1971-12-13");
echo "Dia da semana do meu nascimento foi ".date('l', $ts).'-'.date('d/m/Y', $ts);
echo "<br><br>";

$ts = strtotime('+1 day 3 hours');
echo date('l, d/m/Y', $ts);
echo "<br>";
$ts = strtotime('+1 week');
echo date('l, d/m/Y', $ts);
echo "<br>";
$ts = strtotime('+1 year');
echo date('l, d/m/Y', $ts);
echo "<br>";
?>