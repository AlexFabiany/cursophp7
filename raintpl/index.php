<?php 

	require_once("vendor/autoload.php");

	use Rain\Tpl;
	// config
	$config = array(
	    "tpl_dir"       => "tpl/",
	    "cache_dir"     => "cache/"
	);
	Tpl::configure( $config );
	
	// draw
	$tpl = new Tpl;
	$tpl->assign("name", "Alex Fabiany");
	$tpl->assign("version", PHP_VERSION);
	$tpl->draw( "index" );

?>