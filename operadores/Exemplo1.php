<?php
    $nome = "Alex Fabiany";
    echo $nome." mais alguma coisa<br>";
    
    $nome .= " S. Leão<br>";
    echo $nome;

    $valorTotal = 0;
    $valorTotal += 100;
    $valorTotal += 25;
    echo "Valor Total = ".$valorTotal."</br>";
    echo "Valor Total com desconto de 10% = ".$valorTotal *= .9;
    echo "<br>";
    echo "<br>";
    echo "<br>";

    $a = 10;
    $b = 2;
    echo "a = 10 / b = 2";
    echo "<br>";
    echo "a + b = ".($a + $b);
    echo "<br>";
    echo "a - b = ".($a - $b);
    echo "<br>";
    echo "a * b = ".($a * $b);
    echo "<br>";
    echo "a / b = ".($a / $b);
    echo "<br>";
    echo "a mod b = ".($a % $b);
    echo "<br>";
    echo "a ^ b = ".($a ** $b);
    echo "<br>";
    echo "<br>";
    echo "<br>";

    echo "a = 30 / b = 55";
    $a = 30.0;
    $b = 55;
    echo "<br>";
    echo "a > b? ";
    var_dump($a > $b);
    echo "<br>";
    echo "a < b? ";
    var_dump($a < $b);
    echo "<br>";
    echo "a = b? ";
    var_dump($a == $b);
    echo "<br>";
    echo "Tipo/Valor a = Tipo/Valor b? ";
    var_dump($a === $b);
    echo "<br>";
    echo "a <> b? ";
    var_dump($a != $b);
    echo "<br>";
    echo "Tipo/Valor a <> Tipo/Valor b? ";
    var_dump($a != $b);
    echo "<br>";
?>