CREATE TABLE tb_usuarios(
	idusuario INT NOT NULL IDENTITY PRIMARY KEY,
    login VARCHAR(64) NOT NULL,
    senha VARCHAR(255),
    dtcadastro DATETIME NOT NULL DEFAULT GETDATE()
);
INSERT INTO tb_usuarios(login, senha) VALUES('Alex', '!@#$%');
INSERT INTO tb_usuarios(login, senha) VALUES('sqlserver', 'a1b2c3');

SELECT * FROM tb_usuarios;