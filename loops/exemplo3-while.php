<?php
    $condicao = true;

    while($condicao) {
        $numero = rand(1, 20);
        if($numero === 3){
            echo "Fim do while!!! <br>";
            $condicao = false;
        }
        echo $numero."<br>";
    }
    echo "<br>";

    $total = 150;
    $desconto = 0.9;
    do {
        $total *= $desconto;
        echo $total."<br>";
    } while ($total > 100);
?>