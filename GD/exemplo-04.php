<?php 

	header("Content-Type: image/jpeg");

	$file = "wallpaper.jpg";

	list($old_width, $old_height) = getimagesize($file);

	$new_width = $old_width / 8;
	$new_height = $old_height / 8;

	$new_image = imagecreatetruecolor($new_width, $new_height);
	$old_image = imagecreatefromjpeg($file);

	imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

	imagejpeg($new_image); //, $file."_".$new_width."x".$new_height.".jpg");

	imagedestroy($old_image);

	imagedestroy($new_image);
?>