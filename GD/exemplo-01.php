<?php 

	header("Content-Type: image/png");

	$img = imagecreate(256, 256);

	$preto = imagecolorallocate($img, 0, 0, 0);

	$vermelho = imagecolorallocate($img, 255, 0, 0);

	imagestring($img, 5, 60, 120, "Curso de PHP 7", $vermelho);

	imagepng($img);

	imagedestroy($img);
?>