<?php 

	$filename = "usuarios.csv";

	if(file_exists($filename)){
		$file = fopen($filename, "r");
		$headers = explode(";", fgets($file));
		$dados = array();
		while($row = fgets($file)){

			$rowDados = explode(";", $row);
			$linha = array();
			for($i = 0; $i < count($headers); $i++){
				$linha[$headers[$i]] = $rowDados[$i];
			}
			array_push($dados, $linha);
		}

		fclose($file);
		echo json_encode($dados);
	}

?>