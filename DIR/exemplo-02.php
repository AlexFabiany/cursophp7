<?php 

	$dir = "images";
	$images = scandir($dir);
	$dados = array();

	foreach ($images as $img) {
		if(!in_array($img, array('.', '..'))){

			$filename = $dir.DIRECTORY_SEPARATOR.$img;

			$fileInfo = pathinfo($filename);

			$fileInfo['size'] = filesize($filename);
			$fileInfo['modified'] = date('d/m/Y H:i:s', filemtime($filename));
			$fileInfo['url'] = "http://localhost/DIR/".str_replace("\\", "/", $filename);

			array_push($dados, $fileInfo);
		}
	}

	echo json_encode($dados);

?>