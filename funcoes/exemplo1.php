<?php
  // function ola(){
  //   return "Olá mundo!";
  // }

  // echo ola();

  // $frase = ola();
  // echo strlen($frase);
  // echo "<br>";
  // var_dump($frase);

  // function ola(){
  //   $argumentos = func_get_args();
  //   return $argumentos;
  // }
  // var_dump(ola("Bom dia", "Alex"));

  // $a = 10;
  // function trocaValor(&$param){
  //   $param += 50;
  //   return $param;
  // }
  // echo trocaValor($a);
  // echo "<br>";
  // echo $a;
  // echo "<br>";

  // $pessoa = array(
  //   'nome'=>'Alex',
  //   'idade'=>47
  // );
  
  // foreach ($pessoa as $value) {
  //   if (gettype($value) === 'integer') $value += 10;
  //   echo $value."<br>";
  // }
  // print_r($pessoa);

  // function somar(int ... $valores) {
  //   return array_sum($valores);
  // }

  // echo somar(25, 35, 100);
  // echo "<br>";
  // echo somar(2.5, 3.5, 10.0);
  // echo "<br>";

  function somar(float ... $valores): float {
    return array_sum($valores);
  }

  echo var_dump(somar(25, 35, 100));
  echo "<br>";
  echo var_dump(somar(2.5, 3.5, 10.0));
  echo "<br>";
?>