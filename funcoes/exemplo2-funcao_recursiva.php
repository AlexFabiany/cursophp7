<?php

// TreeView ou Hierarquia
  $arvore = array(
    array(
      'nome_cargo'=>'CEO',
      'subordinados'=>array(
        // Inicio: Diretor Comercial
        array(
          'nome_cargo'=>'Diretor Comercial',
          'subordinados'=>array(
            // Inicio: Gerente de Vendas
            array(
              'nome_cargo'=>'Gerente de Vendas',
              'subordinados'=>array(
                // Inicio: Supervisor de Vendas
                array(
                  'nome_cargo'=>'Supervisor de Vendas'
                )
                // Fim: Supervisor de Vendas
              )
            )
            // Fim: Gerente de Vendas
          )
        ),
        // Fim Diretor Comercial
        // Inicio: Diretor Financeiro
        array(
          'nome_cargo'=>'Diretor Financeiro',
          'subordinados'=>array(
            // Inicio: Gerente de Contas a Receber
            array(
              'nome_cargo'=>'Gerente de Contas a Receber',
              'subordinados'=>array(
                // Inicio: Supervisor de Contas a Receber
                array(
                  'nome_cargo'=>'Supervisor de Contas a Receber'
                )
              )
            ),
            //Fim: Gerente de Contas a Receber
            // Inicio: Gerente de Contas a Pagar
            array(
              'nome_cargo'=>'Gerente de Contas a Pagar',
              'subordinados'=>array(
                // Inicio: Supervisor de Pagamentos
                array(
                  'nome_cargo'=>'Supervisor de Pagamentos',
                )
                // Fim: Supervisor de Pagamentos
              )
            ),
            // Fim: Gerente de Contas a Pagar
            // Inicio: Gerente de Compras
            array(
              'nome_cargo'=>'Gerente de Compras',
              'subordinados'=>array(
                // Inicio: Supervisor de Suprimentos
                array(
                  'nome_cargo'=>'Supervisor de Suprimentos',
                  'subordinados'=>array(
                    // Inicio: Encarregado de Suprimentos
                    array(
                      'nome_cargo'=>'Encarregado de Suprimentos'
                    )
                  )
                  // Fim: Encarregado de Suprimentos
                )
                // Fim: Supervisor de Suprimentos
              )
            )
            // Fim: Gerente de Compras
          )
        )
        // Fim: Diretor Financeiro
      )
    )
  );
  // Exibir a TreeView ou Hierarquia
  function exibirArvore($cargos) {
    $html = "<ul>";

    foreach ($cargos as $cargo) {
    
      $html .= "<li>";

      $html .= $cargo['nome_cargo'];

      if(isset($cargo['subordinados']) && count($cargo['subordinados']) > 0) {

        $html .= exibirArvore($cargo['subordinados']);

      }
      $html .= "</li>";
    }

    $html .= "</ul>";

    return $html;

  }

  echo exibirArvore($arvore);
?>