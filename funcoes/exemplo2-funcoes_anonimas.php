<?php

function executar($callback) {

  // Processo demorado... 
  for ($i=0; $i < 10000; $i++) { 
    echo "Processando...<br>";
  }
  echo "<br>";
  $callback();

}

executar(function() {

  echo "Processo finalizado!";

  }
);

// Outra maneira... 

// $fn = function($a){
//   var_dump($a);
// };

// $fn("Olá mundo!");

?>