<?php 
	class Endereco
	{
		private $logradouro;
		private $numero;
		private $bairro;
		private $cidade;
		private $uf;
		private $cep;

		public function __construct($logr, $num, $bairro, $cidade, $uf, $cep)
		{
			$this->logradouro = $logr;
			$this->numero = $num;
			$this->bairro = $bairro;
			$this->cidade = $cidade;
			$this->uf = $uf;
			$this->cep = $cep;
		}

		public function __destruct()
		{
			//var_dump("Objeto Destruído!");
		}

		public function __toString()
		{
			return $this->logradouro.', '.$this->numero.' - '.$this->bairro.' - '.$this->cidade.'/'.$this->uf.' CEP '.$this->cep;
		}
	}

	$meuEndereco = new Endereco('Rua Francisco Carretti', '231', 'São Jorge', 'Manaus', 'AM', '69.033-180');
	var_dump($meuEndereco);
	//unset($meuEndereco); // Opcional: o PHP destrói automaticamente - Garbage Collector

	echo '<br><br>';
	echo "Endereço: ".$meuEndereco;
	echo '<br>';
 ?>