<?php
	class Carro{
		private $ano;
		private $modelo;
		private $motor;

		public function getAno(): int {
			return $this->ano;
		}

		public function setAno($ano){
			$this->ano = $ano;
		}

		public function getModelo(){
			return $this->modelo;
		}

		public function setModelo($modelo){
			$this->modelo = $modelo;
		}

		public function getMotor(): float {
			return $this->motor;
		}

		public function setMotor($motor){
			$this->motor = $motor;
		}

		public function toString(){
			return array(
				"Ano: "=> $this->getAno(),
				"Modelo: "=> $this->getModelo(),
				"Motor: "=> $this->getMotor()
			);
		}
	}

	$gol = new Carro();
	$gol->setAno('1998');
	$gol->setModelo('Gol Trend');
	$gol->setMotor('1.0');
	var_dump($gol->toString());
	?>