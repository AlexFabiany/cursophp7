<?php 
	abstract class Animal
	{
		public function emitirSom()
		{
			return 'Som...';
		}

		public function moverSe()
		{
			return 'Anda';
		}
	}

	class Cachorro extends Animal
	{
		public function emitirSom()
		{
			return 'Late';
		}
	}

	class Gato extends Animal
	{
		public function emitirSom()
		{
			return 'Mia';
		}
	}

	class Passaro extends Animal
	{
		public function emitirSom()
		{
			return 'Canta';
		}

		public function moverSe()
		{
			return 'Voa e '.parent::moverSe();
		}
	}

	$valente = new Cachorro();
	echo $valente->emitirSom();
	echo '<br/>';
	echo $valente->moverSe();
	echo '<br/>';
	echo '--------------'.'<br/>';
	$bichano = new Gato();
	echo $bichano->emitirSom();
	echo '<br/>';
	echo $bichano->moverSe();
	echo '<br/>';
	echo '--------------'.'<br/>';
	$piupiu = new Passaro();
	echo $piupiu->emitirSom();
	echo '<br/>';
	echo $piupiu->moverSe();
	echo '<br/>';

?>