<?php
  class Pessoa {
    public $nome;

    public function falar() {
      return "O meu nome é ".$this->nome;
    }
  }

  $alex = new Pessoa();
  $alex->nome = "Alex Fabiany";
  echo $alex->falar();
?>