<?php 
interface IVeiculo
	{
		public function acelerar($velocidade);
		public function frear();
		public function trocarMarcha($marcha);
	}
?>