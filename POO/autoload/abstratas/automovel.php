<?php
	interface IVeiculo
	{
		public function acelerar($velocidade);
		public function frear();
		public function trocarMarcha($marcha);
	}
	
	abstract class Automovel implements IVeiculo
	{
		public function acelerar($velocidade)
		{
			return 'O veículo '.get_class($this).' acelerou até '.$velocidade.' Km/h!';
		}

		public function frear()
		{
			return 'O veículo '.get_class($this).' agora está parado!';
		}

		public function trocarMarcha($marcha)
		{
			return 'O veículo '.get_class($this).' trocou para a '.$marcha.' marcha!';
		}
	}
?>