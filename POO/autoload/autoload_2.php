<?php 
	function includeClass($nameClass)
	{
		if(file_exists($nameClass.".php") === true) 
		{
			require_once($nameClass.".php");
		}
	}

	spl_autoload_register("includeClass");

	spl_autoload_register(function($nameClass){
		if(file_exists("abstratas".DIRECTORY_SEPARATOR.$nameClass.".php") === true)
		{
			require_once("abstratas".DIRECTORY_SEPARATOR.$nameClass.".php");
		}
	});

	$carro = new DelRey();
	echo $carro->acelerar(80);
?>