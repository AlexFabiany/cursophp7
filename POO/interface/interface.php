<?php 
	interface IVeiculo
	{
		public function acelerar($velocidade);
		public function frear();
		public function trocarMarcha($marcha);
	}

	class Gol implements IVeiculo
	{
		public function acelerar($velocidade)
		{
			return 'O veículo acelerou até '.$velocidade.' Km/h!';
		}

		public function frear()
		{
			return 'O veículo agora está parado!';
		}

		public function trocarMarcha($marcha)
		{
			return 'O veículo trocou para a '.$marcha.' marcha!';
		}
	}

	$carro = new Gol();
	echo $carro->acelerar('50');
	echo '<br/>';
	echo $carro->trocarMarcha('5ª');
	echo '<br/>';
	echo $carro->frear();
	echo '<br/>';
?>