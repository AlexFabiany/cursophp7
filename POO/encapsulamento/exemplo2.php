<?php 
	class Pessoa
	{
		public $nome = 'Rasmus Lerdorf';
		protected $idade = 48;
		private $senha = '123456';

		public function verDados()
		{
			echo 'Nome da classe: '.get_class($this).'<br/>';
			echo 'Nome: '.$this->nome . '<br/>';
			echo 'Idade: '.$this->idade . '<br/>';
			echo 'Senha: '.$this->senha . '<br/>';
		}
	}

	class Programador extends Pessoa
	{
		public function verDados()
		{
			echo 'Nome da classe: '.get_class($this).'<br/>';
			echo 'Nome: '.$this->nome . '<br/>';
			echo 'Idade: '.$this->idade . '<br/>';
			echo 'Senha: '.$this->senha . '<br/>';
		}
	}

	$alex = new Programador();
	$alex->verDados();
	// echo $alex->nome.'<br/>';
	// echo $alex->idade.'<br/>'; // Erro fatal: idade é protected
?>