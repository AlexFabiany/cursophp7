<?php 
	interface IVeiculo
	{
		public function acelerar($velocidade);
		public function frear();
		public function trocarMarcha($marcha);
	}

	abstract class Automovel implements IVeiculo
	{
		public function acelerar($velocidade)
		{
			return 'O veículo '.get_class($this).' acelerou até '.$velocidade.' Km/h!';
		}

		public function frear()
		{
			return 'O veículo '.get_class($this).' agora está parado!';
		}

		public function trocarMarcha($marcha)
		{
			return 'O veículo '.get_class($this).' trocou para a '.$marcha.' marcha!';
		}
	}

	class DelRey extends Automovel
	{
		public function empurrar()
		{
			return 'O '.get_class($this).' pregou! Empurrando... :|';
		}
	}

	$carro = new DelRey();
	echo $carro->acelerar(50);
	echo '<br/>';
	echo $carro->trocarMarcha('3ª');
	echo '<br/>';
	echo $carro->frear();
	echo '<br/>';
	echo $carro->empurrar();
	echo '<br/>';
?>