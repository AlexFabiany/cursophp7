<!DOCTYPE html>
<html>
	<meta charset="utf-8">
	<head>
		
		<title>Segurança: Cross Site Scripting - XSS</title>
	</head>
	<body>

		<form method="post">

			<input type="text" name="busca">
			<button type="submit">Enviar</button>

		</form>

	</body>

</html>

<?php 

	if(isset($_POST["busca"]))
	{
		//echo strip_tags($_POST["busca"], "<strong><a>"); //strip_tags: remove todas as tags do campo, exceto "<strong><a>"

		echo htmlentities($_POST["busca"]); // transforma todas as tags em texto
	}
?>