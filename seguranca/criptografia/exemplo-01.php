<?php

	define('SECRET', pack('a16', 'senha'));
	define('SECRET_IV', pack('a16', 'senha'));
	
	$data = [
		"nome" => "Alex Fabiany"
	];

	$openssl = openssl_encrypt(
		json_encode($data), 
		"AES-128-CBC", 
		SECRET, 
		0, 
		SECRET_IV
	);

	echo $openssl;

	$string = openssl_decrypt(
		$openssl, 
		"AES-128-CBC", 
		SECRET, 
		0, 
		SECRET_IV
	);

	var_dump(json_decode($string, true));

	/* mcrypt_???: depreciado a partir da versão 7.1.0
	define('SECRET', pack('a16', 'senha'));

	$mcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128,	SECRET,	json_encode($data),	MCRYPT_MODE_ECB);

	var_dump(base64_encode($mcrypt));
	*/

?>
